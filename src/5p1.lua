require "llib"

local inp = llib.io.readfile("5.input")

local lines = {}
for s in inp:gmatch("[^\r\n]+") do
    table.insert(lines, s)
end

local seeds = {}
local starting_row = 0
local state = 0
local tnum = ""
for i=1,#lines do
  for ci=1,string.len(lines[i]) do
    local c = string.sub(lines[i],ci,ci)
    if c == ':' then
      state = 1
    elseif c == 's' and state ~= 0 then
      state = -1
      break
    elseif state == 1 then
      if c == ' ' and tnum:gsub("%s+","") ~= "" then
        table.insert(seeds, tonumber(tnum))
        tnum = ""
      else
        tnum = tnum .. c
      end
    end
  end

  if state == -1 then        --print(tnum)
    starting_row = i
    table.insert(seeds, tonumber(tnum))
    break
  end
end
--seeds have been parsed

state = 0
tnum = ""
local steps = {}
local step = {}
local operation = {}
for i = starting_row,#lines do
  for ci=1,string.len(lines[i]) do
    local c = string.sub(lines[i],ci,ci)
    if c == ':' then
      state = 1
    elseif tonumber(c..'5') == nil and state ~= 0 then
      --print operation
      table.insert(steps,step)
      step = {}
      operation = {}
      state = 0
    elseif state == 1 then
      if c == ' ' and tnum:gsub("%s+","") ~= "" then
        table.insert(operation, tonumber(tnum))
        tnum = ""
      else
        tnum = tnum .. c
      end
    end
  end

  table.insert(operation, tonumber(tnum))
  tnum = ""
  if #operation == 3 then
    table.insert(step,operation)
    operation = {}
  end
end
table.insert(steps,step)
--steps parsed

local lowest = nil
for i=1,#seeds do
  for a=1,#steps do
    for b=1,#steps[a] do
      if steps[a][b][2] <= seeds[i] and seeds[i] < steps[a][b][2] + steps[a][b][3] then
        --print(seeds[i] .. " for " .. steps[a][b][1] .. " " .. steps[a][b][2] .. " " .. steps[a][b][3])
        seeds[i] = seeds[i] + (steps[a][b][1] - steps[a][b][2])
        break
      end
    end
  end
  if lowest == nil then lowest = seeds[i] end
  if seeds[i] < lowest then
    lowest = seeds[i]
  end
end
print(lowest)
