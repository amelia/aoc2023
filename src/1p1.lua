require "llib"

local inp = llib.io.readfile("1.input")

local lines = {}
for s in inp:gmatch("[^\r\n]+") do
    table.insert(lines, s)
end

local total = 0
for _,s in pairs(lines) do
  local firstn = -1;
  local lastn = 0;
  for c in s:gmatch"." do
    if not (tonumber(c) == nil) then
      if firstn == -1 then
        firstn = tonumber(c);
      end
      lastn = tonumber(c);
    end
  end
  total = total + (firstn * 10) + lastn;
end
print(total)
