require "llib"

local inp = llib.io.readfile("4.input")

lines = {}
for s in inp:gmatch("[^\r\n]+") do
    table.insert(lines, s)
end

local cards = {}
local total_cards = {}
local solved = {}
local function iter(i) 
  local score = 0
  local winning = {}
  local state = 0
  local cnum = ""
  for ci=1,#lines[i] do
    --print(string.sub(lines[i],ci,ci))
    local c = string.sub(lines[i],ci,ci)
    if c == ':' then
      state = 1
    elseif c == '|' then
      state = 2
      cnum = ""
    else
      if state == 1 then
        if c == " " then
          table.insert(winning, tonumber(cnum))
          cnum = ""
        else
          cnum = cnum .. c
        end
      elseif state == 2 then
        if c == " " and cnum:gsub("%s+","") ~= "" then
          if (llib.array.index(winning, cnum) ~= -1) then
            score = score + 1
          end
          cnum = ""
        else
          cnum = cnum ..c
        end
      end
    end
  end

  if (llib.array.index(winning,cnum) ~= -1) then
    score = score + 1
  end

  return score
end
for i=1,#lines do
  cards[i] = 1
  total_cards[i] = 1
end

local total = 0
while llib.array.sum(cards) > 0 do
  for i=1,#lines do
    if cards[i] > 0 then
      if solved[i] == nil then 
        solved[i] = iter(i)
      end
      local score = solved[i]--iter(i)

      total = total + score
      for l=(i),(i+score) do
        if cards[l] ~= nil and l ~= i then
          cards[l] = cards[l] + 1
          total_cards[l] = total_cards[l] + 1
        end
      end
      cards[i] = cards[i] - 1
    end
  end
end
print(llib.array.sum(total_cards))

