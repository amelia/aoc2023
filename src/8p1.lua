require "llib"

local inp = llib.io.readfile("8.input")

local lines = {}
for s in inp:gmatch("[^\r\n]+") do
    table.insert(lines, s)
end

local instruction = lines[1]
local instruction_line = 1
local map = {}
for i=2,#lines do
  local pos = string.sub(lines[i],1,3)
  local l = string.sub(lines[i],8,10)
  local r = string.sub(lines[i],13,15)
  map[pos] = {pos=pos,L=l,R=r}
end

local current = map["AAA"]
local iter = 0
while current.pos ~= "ZZZ" do
  current = map[current[string.sub(instruction,instruction_line,instruction_line)]]
  if instruction_line == string.len(instruction) then
    instruction_line = 0
  end
  instruction_line = instruction_line + 1
  iter = iter + 1
end
print(iter)
