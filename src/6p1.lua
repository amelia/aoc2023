require "llib"

local inp = llib.io.readfile("6.input")

local lines = {}
for s in inp:gmatch("[^\r\n]+") do
    table.insert(lines, s.." ")
end

local function get_row(ind)
  local tfun = {}
  local nnum = ""
  for i=1,string.len(lines[ind]) do
    local c = string.sub(lines[ind],i,i)
    if c == ' ' then
      if tonumber(nnum) ~= nil then
        table.insert(tfun, tonumber(nnum))
      end
      nnum = ""
    else
      nnum = nnum .. c
    end
  end
  return tfun
end

local times = get_row(1)
local highscores = get_row(2)

local total = 1
for i=1,#times do
  local better = 0
  for z=1,times[i] - 1 do
    if z*(times[i]-z) > highscores[i] then
      better = better + 1
    end
  end

  if better ~= 0 then
    total = total * better
  end
end
print(total)
