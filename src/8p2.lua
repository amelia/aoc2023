require "llib"

local inp = llib.io.readfile("8.input")

local lines = {}
for s in inp:gmatch("[^\r\n]+") do
    table.insert(lines, s)
end

local instruction = lines[1]
local instruction_line = 1
local map = {}
local track = {}
for i=2,#lines do
  local pos = string.sub(lines[i],1,3)
  local l = string.sub(lines[i],8,10)
  local r = string.sub(lines[i],13,15)
  map[pos] = {pos=pos,L=l,R=r}
  if string.sub(pos,3,3) == "A" then
    table.insert(track,map[pos])
  end
end

local values = {}
local iter = 0
for i=1,#track do
  instruction_line = 1
  local l_iter = 0
  while string.sub(track[i].pos,3,3) ~= "Z" do
    track[i] = map[track[i][string.sub(instruction,instruction_line,instruction_line)]]
    if instruction_line == string.len(instruction) then
      instruction_line = 0
    end
    instruction_line = instruction_line + 1
    l_iter = l_iter + 1
  end
  table.insert(values,l_iter)
end

print(llib.math.lcm(values))
