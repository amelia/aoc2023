require "llib"

local inp = llib.io.readfile("6.input")

local lines = {}
for s in inp:gmatch("[^\r\n]+") do
    table.insert(lines, s.." ")
end

local function get_row(ind)
  local tfun = 0
  local nnum = ""
  for i=1,string.len(lines[ind]) do
    local c = string.sub(lines[ind],i,i)
    if c == ' ' then
      if tonumber(nnum) ~= nil then
        tfun = tfun * 10^string.len(nnum) + nnum
      end
      nnum = ""
    else
      nnum = nnum .. c
    end
  end
  --print(tfun)
  return tfun
end

local times = get_row(1)
local highscores = get_row(2)

--os.exit(0)
local better = 0
for z=1,times - 1 do
    --print(z*(times[i]-z))
  if z*(times-z) > highscores then
    better = better + 1
  end
end

print(better)
