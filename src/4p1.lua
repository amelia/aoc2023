require "llib"

local inp = llib.io.readfile("4.input")

lines = {}
for s in inp:gmatch("[^\r\n]+") do
    table.insert(lines, s)
end

local total = 0
for i=1,#lines do
  local score = 0
  local winning = {}
  local state = 0
  local cnum = ""
  for ci=1,#lines[i] do
    --print(string.sub(lines[i],ci,ci))
    local c = string.sub(lines[i],ci,ci)
    if c == ':' then
      state = 1
    elseif c == '|' then
      state = 2
      cnum = ""
    else
      if state == 1 then
        if c == " " then
          table.insert(winning, tonumber(cnum))
          cnum = ""
        else
          cnum = cnum .. c
        end
      elseif state == 2 then
        if c == " " and cnum:gsub("%s+","") ~= "" then
          if (llib.array.index(winning, cnum) ~= -1) then
            if score == 0 then
              score = 1
            else
              score = score*2
            end
          end
          cnum = ""
        else
          cnum = cnum ..c
        end
      end
    end
  end

  if (llib.array.index(winning,cnum) ~= -1) then
    if score == 0 then
      score = 1
    else
      score = score*2
    end
  end
  
  total = total + score
  --print(score)
  --[[
  for z=1,#winning do
    print(winning[z])
  end
  print(cnum)
  ]]
end

print(total)
