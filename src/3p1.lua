require "llib"

local inp = llib.io.readfile("3.input")

lines = {}
for s in inp:gmatch("[^\r\n]+") do
    table.insert(lines, s)
end

function issym(a)
  if a == '.' then
    return false
  end

  if tonumber(a) == nil then
    return true
  end

  return false
end

tracked = {}
function iter(ux, uy, ym)
  if ux > 0 and uy > 0 and not(tonumber(string.sub(lines[ux],uy,uy)) == nil) then 
    local fullnum = ""-- = string.sub(lines[ux],uy,uy)
    local start_x = 0;
    for i=uy+ym,0,-1 do
      if tonumber(string.sub(lines[ux],i,i)) == nil then
        start_x = i+1;
        break;
      end
    end

    for i,_ in pairs(tracked) do
      if(tracked[i].x == ux and tracked[i].y == start_x) then
        return 0;
      end
    end
    table.insert(tracked, {x=ux,y=start_x});

    for i=start_x,string.len(lines[ux]) do
      if tonumber(string.sub(lines[ux],i,i)) == nil then
        break;
      end
      fullnum = fullnum .. string.sub(lines[ux],i,i)
    end
    --out = out + fullnum
    --print(fullnum)
    return tonumber(fullnum);
  end
  return 0;
end
function s_adj(x,y)
  local out = 0;
  out = out + iter(x+1,y,-1)
  out = out + iter(x-1,y,-1)
  out = out + iter(x, y -1, -1)
  out = out + iter(x, y+1, -1)

  out = out + iter(x + 1, y + 1, -1)
  out = out + iter(x - 1, y + 1, -1);
  out = out + iter(x - 1, y - 1, -1);
  out = out + iter(x + 1, y - 1, -1);
  
  return out;
end

total = 0;
for i,_ in pairs(lines) do
  local ind = 1
  for s in lines[i]:gmatch"." do
    if(issym(s)) then
      --print(i.." "..ind)
      total = total + s_adj(i,ind);
    end
    ind = ind + 1
  end
end

print(total)
