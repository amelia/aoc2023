require "llib"

local inp = llib.io.readfile("1.input")

local function find_pairs(s)
  local list = {"one","two","three","four","five","six","seven","eight","nine"};
  local low = {v=nil,ind=string.len(s)+1};
  local high = {v=nil,ind=-1};

  for k,i in pairs(list) do
    local ind = string.find(s, i, 1, true);

    while not (ind == nil) do
      if ind < low.ind then
        low.ind = ind;
        low.v = k;
      end

      if ind > high.ind then
        high.ind = ind;
        high.v = k;
      end

      ind = string.find(s, i, ind+1, true);
    end
  end
  return {low = low, high = high};
end

local lines = {}
for s in inp:gmatch("[^\r\n]+") do
    table.insert(lines, s)
end

local total = 0
for _,s in pairs(lines) do
  local firstn_sel = false;
  local firstn = 0;
  local lastn = 0;

  local m = find_pairs(s);
  if not (m.low.v == nil) then
    firstn = m.low.v
  end
  if not (m.high.v == nil) then
    lastn = m.high.v
  end
  local ind = 1;
  for c in s:gmatch"." do
    if not (tonumber(c) == nil) then
      if ind <= m.low.ind and firstn_sel == false then
        firstn = tonumber(c);
        firstn_sel = true;
      end

      if ind >= m.high.ind then
       lastn = tonumber(c);
      end
    end
    ind=ind+1;
  end
  total = total + (firstn * 10) + lastn;
end
print(total)
