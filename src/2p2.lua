require "llib"

local inp = llib.io.readfile("2.input")

function max(A,B)
  if A < B then
    return B
  end
  return A
end

local lines = {}
for s in inp:gmatch("[^\r\n]+") do
    table.insert(lines, s)
end

local total = 0
for i,s in pairs(lines) do
  local game = {head = ""}
  local hand = {red = 0, blue = 0, green = 0}
  local nbuffer = ""
  local buffer = ""
  local state = 0
  for c in s:gmatch"." do
    if c == ':' then
      state = 1
    end
    if state == 0 and (not (game["head"] == "") or c == " ")then
      game["head"] = game["head"]..c;
    end

    if not(state == 0) and not (c == ':') then
      if state == 1 then
        if c == ' ' and not (nbuffer == "") then
          state = 2
        else
          nbuffer = nbuffer..c
        end
      end

      if state == 2 and not (c == ' ') then
        if c == ',' then
          state = 1
          hand[buffer] = max(hand[buffer], tonumber(nbuffer));
          nbuffer = ""
          buffer = ""
        elseif c == ';' then
          hand[buffer] = max(hand[buffer], tonumber(nbuffer));
          state = 1
          nbuffer = ""
          buffer = ""
        else
          buffer = buffer..c
        end
      end
    end
  end
  hand[buffer] = max(hand[buffer], tonumber(nbuffer));
  total = total + hand.red * hand.blue * hand.green
end
print(total)
